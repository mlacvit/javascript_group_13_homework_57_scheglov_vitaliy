import { Component, ElementRef, Output, ViewChild } from '@angular/core';
import { UserServices } from '../user/user.services';
import { GroupModel, UserModel } from '../user/user.model';


@Component({
  selector: 'app-new-users',
  templateUrl: './new-users.component.html',
  styleUrls: ['./new-users.component.css']
})
export class NewUsersComponent  {
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput') emailInput!: ElementRef;
  @ViewChild('checkInput') checkInput!: ElementRef;
  @ViewChild('select') select!: ElementRef;
  @ViewChild('groupInput') groupInput!: ElementRef;

  constructor(public userService: UserServices, public userGroup: UserServices) { }

  addValueUser(){
    const name = this.nameInput.nativeElement.value;
    const email = this.emailInput.nativeElement.value;
    const select = this.select.nativeElement.value;
    let check = '';
    if (this.checkInput.nativeElement.checked){
      check = 'active';
    }else {check = 'no active'}
    const userModel = new UserModel(name, email, check, select);
    this.userService.users.push(userModel);
  }

  addValueGroup(){
    const group: UserModel = {checkBox: '', select: '', name: '', email: ''};
    this.userGroup.group.push(group);
    this.userGroup.nameGroup = {nameGroup: this.groupInput.nativeElement.value};
  }

}
