import { Component, Input } from '@angular/core';
import { UserServices } from './user/user.services';
import { UserModel } from './user/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(public userServices: UserServices, public userGroup: UserServices) {}


  addNameGroup(obj: UserModel) {
    return this.userGroup.nameGroup;
  }

  addToGroup(obj: UserModel) {
    this.userGroup.group.push(obj);
  }
}
