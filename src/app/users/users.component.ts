import { Component, EventEmitter, Input, Output } from '@angular/core';
import { UserModel } from '../user/user.model';
import { UserServices } from '../user/user.services';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent  {
  @Input() massUser!: UserModel;
  @Input() UserClick!: UserModel[];
constructor(public group: UserServices) {
}

  colorRole() {
    let color = '';
    if (this.massUser.select === 'user'){
       color = 'green';
    }if (this.massUser.select === 'admin'){
       color = 'red';
    }if (this.massUser.select === 'editor'){
       color = 'blue';
    }
    return color;
  }

  activeStatus() {
   let colorStatus = '';
    if (this.massUser.checkBox === 'active'){
      colorStatus = 'green';
    }else {colorStatus = 'red';}
    return colorStatus;
  }

}
