import { Component, Input } from '@angular/core';
import { UserServices } from '../user/user.services';
import { GroupModel } from '../user/user.model';


@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent  {
  @Input() group!: GroupModel;
  constructor(public nameGroup: UserServices) {
  }
}

