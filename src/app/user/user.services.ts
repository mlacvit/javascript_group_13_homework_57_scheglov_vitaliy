import { UserModel,  GroupModel} from './user.model';

export class UserServices {
  users: UserModel[] = [
    {checkBox: 'active', select: 'user', name: 'William', email: 'wiillAm@yahoo.com'},
    {checkBox: 'no active', select: 'admin', name: 'John', email: 'johnDoe@gmail.com'},
    {checkBox: 'active', select: 'editor', name: 'Alex', email: 'alex85@gmail.com'},
  ];
  group: UserModel[] = [];
  nameGroup: GroupModel = {nameGroup: ''};
}
