import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { NewUsersComponent } from './new-users/new-users.component';
import { UsersComponent } from './users/users.component';
import { UserServices } from './user/user.services';
import { FormsModule } from '@angular/forms';
import { GroupComponent } from './group/group.component';

@NgModule({
  declarations: [
    AppComponent,
    NewUsersComponent,
    UsersComponent,
    GroupComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [UserServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
